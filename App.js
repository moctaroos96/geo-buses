import React from 'react';
import * as Location from 'expo-location'
import * as TaskManager from 'expo-task-manager'
import * as Permissions from 'expo-permissions';
import { Text, TouchableOpacity,View, StyleSheet,YellowBox } from 'react-native';
import MapView, {
  PROVIDER_GOOGLE
} from 'react-native-maps';
import SocketIOClient from 'socket.io-client';
import Modal from 'react-native-modal';

// const socket = SocketIOClient.connect('http://192.168.0.115:7000');

const socket = SocketIOClient.connect('http://bus.boki.tech');

YellowBox.ignoreWarnings([
  'Unrecognized WebSocket connection option(s) `agent`, `perMessageDeflate`, `pfx`, `key`, `passphrase`, `cert`, `ca`, `ciphers`, `rejectUnauthorized`. Did you mean to put these under `headers`?'
]);

const LOCATION_TASK_NAME = 'background-location-task';

let locations001;
let tripid001;
let date001;
let status001;


export default class Geo extends React.Component {

  constructor(props){
    super(props);

    this.state={
      hasLocationPermissions: false,
      errorMessage: null,
      visibleModal: null,
    }

  }

  _getLocationAsync = async () => {
    let { status } = await Permissions.askAsync(Permissions.LOCATION);
    if (status !== 'granted') {
      this.setState({
        errorMessage: 'Permission to access location was denied',
      });
    } else {
      this.setState({ hasLocationPermissions: true });
    }
   }

   componentDidMount(){
    socket.on('connect', ()=>console.log('Connection'));
     this._getLocationAsync()
   }


 
  onPress =  async () => {
    this.setState({ visibleModal: 1 })
    date001 = new Date();
    status001 = "en cours";

    fetch('http://bus.boki.tech/status0', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        status: status001,
        date: date001,
      })
    }).then((response)=> response.json())
    .then((res)=>{
      if(res.creation){
        tripid001=res.tripid_retour
        Location.startLocationUpdatesAsync(LOCATION_TASK_NAME, {
          accuracy: Location.Accuracy.Highest,
          timeInterval: 1000,
        });
      }
    })
    .done()
  };

  onPress1 = async () => {

     this.setState({ visibleModal: null })

    fetch('http://bus.boki.tech/status1', {
            method: 'POST',
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json',
            },
            body: JSON.stringify({
              tripid: tripid001
            })
          })
          .done();

  };

  render() {
    if (this.state.errorMessage) {
      alert(this.state.errorMessage);
    } else{
    return (
         <View style={styles.container}>
            <MapView
              style={styles.map}
              provider={PROVIDER_GOOGLE}
              region={{
                latitude: 14.6937000,
                longitude: -17.4440600,
                latitudeDelta: 0.05,
                longitudeDelta: 0.02
              }}
            />
             <View style={styles.buttonContainer}>
              <TouchableOpacity onPress={this.onPress}>
                  <View style={styles.button}>
                      <Text>START</Text>
                  </View>
              </TouchableOpacity>
                  <Modal isVisible={this.state.visibleModal === 1}>
                  <View style={styles.modalContent}>
                        <Text style={{fontSize: 30, fontStyle: "italic"}}>Starting..........!</Text>
                        <TouchableOpacity onPress={this.onPress1}>
                              <View style={styles.button2}>
                                  <Text>STOP</Text>
                              </View>
                        </TouchableOpacity>
                    </View>
                 </Modal> 
            </View>
      </View>
    );
  }
}
}

TaskManager.defineTask(LOCATION_TASK_NAME, ({ data, error }) => {
  if (error) {
    return error;
  }
  if (data) {
       locations001  = data;
     console.log(locations001)
    socket.emit('message',{data1: locations001, tripid: tripid001});
  }
});

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    justifyContent: "flex-end",
    alignItems: "center"
  },
  map: {
    ...StyleSheet.absoluteFillObject
  },
  bubble: {
    flex: 1,
    backgroundColor: "rgba(255,255,255,0.7)",
    paddingHorizontal: 18,
    paddingVertical: 12,
    borderRadius: 20
  },
  latlng: {
    width: 200,
    alignItems: "stretch"
  },
  button2: {
    padding: 12,
    margin: 16,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)',
    backgroundColor: "#222f3e",
  },
  buttonContainer: {
    flexDirection: "row",
      marginVertical: 10,
    backgroundColor: "transparent",
    padding: 10
  },
  button: {
    backgroundColor: 'white',
    padding: 12,
    margin: 16,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  modalContent: {
    backgroundColor: 'white',
    padding: 22,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
});
